import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Stub interface that represents a {@link Remote} object can be called remotely from another JVM instance.
 * This API call could be used to update a news object that exists in the remote side.
 */
public interface WriteNews extends Remote {
    String WRITE_NEWS_API_REGISTRY_NAME = "write_news";

    /**
     * Updates the news object that exists in the remote side with a given value.
     *
     * @param request : a {@link WriteNewsRequest} instance that has all the arguments required to execute
     *                this operation including the new value for the news object.
     *
     * @return a {@link WriteNewsResponse} that is the response from the response side.
     * @throws {@link RemoteException} if something went wrong on the remote side.
     */
    WriteNewsResponse writeNews(WriteNewsRequest request) throws RemoteException;
}
