import java.io.Serializable;

public class ReadNewsRequest implements Serializable{
    private static final long serialVersionUID = 227L;

    private int clientId;

    public ReadNewsRequest(int clientId) {
        this.clientId = clientId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }
}
