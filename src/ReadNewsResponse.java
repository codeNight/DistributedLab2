import java.io.Serializable;

public class ReadNewsResponse implements Serializable{
    private static final long serialVersionUID = 327L;

    private int news;
    private int reqSeqNum;
    private int serSeqNum;

    public ReadNewsResponse(int news, int reqSeqNum, int serSeqNum) {
        this.news = news;
        this.reqSeqNum = reqSeqNum;
        this.serSeqNum = serSeqNum;
    }

    public int getReqSeqNum() {
        return reqSeqNum;
    }

    public void setReqSeqNum(int reqSeqNum) {
        this.reqSeqNum = reqSeqNum;
    }

    public int getNews() {
        return news;
    }

    public void setNews(int news) {
        this.news = news;
    }

    public int getSerSeqNum() {
        return serSeqNum;
    }

    public void setSerSeqNum(int serSeqNum) {
        this.serSeqNum = serSeqNum;
    }
}
