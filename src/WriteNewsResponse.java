import java.io.Serializable;

public class WriteNewsResponse implements Serializable{
    private static final long serialVersionUID = 927L;

    private int reqSeqNum;
    private int serSeqNum;

    public WriteNewsResponse(int reqSeqNum, int serSeqNum) {
        this.reqSeqNum = reqSeqNum;
        this.serSeqNum = serSeqNum;
    }

    public int getReqSeqNum() {
        return reqSeqNum;
    }

    public void setReqSeqNum(int reqSeqNum) {
        this.reqSeqNum = reqSeqNum;
    }

    public int getSerSeqNum() {
        return serSeqNum;
    }

    public void setSerSeqNum(int serSeqNum) {
        this.serSeqNum = serSeqNum;
    }
}
