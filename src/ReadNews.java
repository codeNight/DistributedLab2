import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Stub interface that represents a {@link Remote} object can be called remotely from another JVM instance.
 * This API call could be used to fetch a news object that exists in the remote side.
 */
public interface ReadNews extends Remote {
    String READ_NEWS_API_REGISTRY_NAME = "read_news";

    /**
     * Asks the server to return that news instance that exists at the moment.
     *
     * @param request : a {@link ReadNewsRequest} instance that has all the arguments required to execute
     *                this operation.
     *
     * @return a {@link ReadNewsResponse} object that contains the news object;
     * @throws {@link RemoteException} if something went wrong on the remote side.
     */
     ReadNewsResponse getNews(ReadNewsRequest request) throws RemoteException;
}
