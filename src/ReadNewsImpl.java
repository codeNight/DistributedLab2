import java.rmi.RemoteException;
import java.util.concurrent.atomic.AtomicInteger;

public class ReadNewsImpl implements ReadNews {
    private static final int SLEEP_TIME = 1000;
    private final Logger readerLogger = Loggers.getReadLogger();

    private AtomicInteger news;
    private AtomicInteger serSeqNum;
    private AtomicInteger reqSeqNum;
    private AtomicInteger readersCount;


    public ReadNewsImpl(AtomicInteger news, AtomicInteger reqSeqNum, AtomicInteger serSeqNum) {
        this.serSeqNum = serSeqNum;
        this.reqSeqNum = reqSeqNum;
        this.news = news;

        this.readersCount = new AtomicInteger(0);
    }


    @Override
    public ReadNewsResponse getNews(ReadNewsRequest request) throws RemoteException{
        readersCount.incrementAndGet();
        int reqSeqNum = this.reqSeqNum.incrementAndGet();

        try {
            Thread.sleep(SLEEP_TIME);
            int serSeqNum = this.serSeqNum.incrementAndGet();
            int news = this.news.get();

            readerLogger.put(String.format("%d %d %d %d",
                    serSeqNum, news, request.getClientId(), readersCount.get()));

            readersCount.decrementAndGet();
            return new ReadNewsResponse(news, reqSeqNum, serSeqNum);
        } catch (Exception e) {
            throw new RemoteException("Failed to read news", e);
        }
    }
}
