import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Run with external library:
 * javac -cp path/to/jsch-0.1.54.jar Start.java
 * java -cp .:path/to/jsch-0.1.54.jar Start
 * <p>
 * Don't forget the system.properties
 * <p>
 * <p>
 * steps:
 * 1- Ensure all the hosts have the same password
 * 2- Ensure all hosts already has the files
 * 3- Adjust the path to the correct one on the machine
 * 4- Compile and run Start.java with the external jar
 */

public class Start {

    private static final String NUM_OF_READERS_KEY = "RW.numberOfReaders";
    private static final String NUM_OF_WRITERS_KEY = "RW.numberOfWriters";
    private static final String NUM_OF_ACCESSES_KEY = "RW.numberOfAccesses";
    private static final String SERVER_PORT_KEY = "RW.server.port";
    private static final String SERVER_KEY = "RW.server";
    private static final String READER_KEY = "RW.reader";
    private static final String WRITER_KEY = "RW.writer";
    private static final String PASSWORD_KEY = "RW.password";

    public static void main(String[] args) {

        Start start = new Start();
        Properties prop = start.readProperties();

        String password = prop.getProperty(PASSWORD_KEY);
        String port = prop.getProperty(SERVER_PORT_KEY);
        String serverIP = prop.getProperty(SERVER_KEY).substring(prop.getProperty(SERVER_KEY).indexOf('@') + 1);

        int numOfAccesses = Integer.parseInt(prop.getProperty(NUM_OF_ACCESSES_KEY));
        int numOfReaders = Integer.parseInt(prop.getProperty(NUM_OF_READERS_KEY));
        int numOfWriters = Integer.parseInt(prop.getProperty(NUM_OF_WRITERS_KEY));

        String commandChangePath = "cd ~/DistributedLab2;";            // FIXME adjust to the correct path

        String compileCommand = "javac -cp lib/jsch-0.1.54.jar -d build/classes -sourcepath src src/*.java;";

        String commandCompileAndRunServer = compileCommand + "java -cp build/classes -Djava.security.policy=resources/server.policy Server " +
                serverIP + " " + port + ";";

        String commandCompileAndRunClient = compileCommand + "java -cp build/classes Client "
                + serverIP + " " + port + " " + numOfAccesses + " "; // add: id + " " + type + ";\n"

        start.sshExecuteOnHost(prop.getProperty(SERVER_KEY), password, commandChangePath + commandCompileAndRunServer);

        for (int i = 0; i < numOfReaders; ++i) {
            String host = prop.getProperty(READER_KEY + i);
            String command = commandChangePath + commandCompileAndRunClient + i + " 1;";
            start.sshExecuteOnHost(host, password, command);
        }

        for (int i = 0; i < numOfWriters; ++i) {
            String host = prop.getProperty(WRITER_KEY + i);
            String command = commandChangePath + commandCompileAndRunClient
                    + i + " 0;";
            start.sshExecuteOnHost(host, password, command);
        }


    }

    public Properties readProperties() {
        try {
            File file = new File("resources/system.properties");
            FileInputStream fileInput = new FileInputStream(file);
            Properties properties = new Properties();
            properties.load(fileInput);
            fileInput.close();

            return properties;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    public void sshExecuteOnHost(String host, String password, String command) {
        command = command + "exit;\n";
        System.out.println(command);
        System.out.println(host);
        String user = host.substring(0, host.indexOf('@'));    // user
        host = host.substring(host.indexOf('@') + 1);        // ip

        try {
            JSch jsch = new JSch();

            System.out.println(user);
            System.out.println(host);
            Session session = jsch.getSession(user, host, 22);

            session.setPassword(password);

            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);

            session.connect(30000);

            ChannelExec channel = (ChannelExec) session.openChannel("exec");
//            channel.setOutputStream(System.out);
            channel.setCommand(command);

            channel.run();
            channel.connect(3000);

            channel.disconnect();
            session.disconnect();
        } catch (JSchException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }
}
