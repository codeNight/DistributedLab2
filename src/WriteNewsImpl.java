import java.rmi.RemoteException;
import java.util.concurrent.atomic.AtomicInteger;

public class WriteNewsImpl implements WriteNews {
    private static final int SLEEP_TIME = 1500;
    private final Logger writerLogger = Loggers.getWriterLogger();

    private AtomicInteger news;
    private AtomicInteger serSeqNum;
    private AtomicInteger reqSeqNum;

    public WriteNewsImpl(AtomicInteger news, AtomicInteger reqSeqNum, AtomicInteger serSeqNum) {
        this.news = news;
        this.reqSeqNum = reqSeqNum;
        this.serSeqNum = serSeqNum;
    }

    @Override
    public WriteNewsResponse writeNews(WriteNewsRequest request) throws RemoteException{
        int reqSeqNum = this.reqSeqNum.incrementAndGet();
        try {
            Thread.sleep(SLEEP_TIME);
            this.news.set(request.getNews());

            int serSeqNum = this.serSeqNum.incrementAndGet();

            writerLogger.put(String.format("%d %d %d",
                    serSeqNum, request.getNews(), request.getClientId()));

            return new WriteNewsResponse(reqSeqNum, serSeqNum);
        } catch (Exception e) {
            throw new RemoteException("Failed to write news", e);
        }
    }
}
