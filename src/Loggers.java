import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Loggers {
    private static final String READERS_LOG_FILE = "readers.txt";
    private static final String WRITERS_LOG_FILE = "writers.txt";
    private static final String ENCODING = "UTF-8";

    private static Logger readLogger = null;
    private static Logger writerLogger = null;

    public static Logger getReadLogger() {
        if (readLogger == null) {
            try {
                readLogger = new DefaultLogger(READERS_LOG_FILE, ENCODING);
            } catch (Exception e) {
                throw new IllegalStateException("Failed to initialize loggers", e);
            }
        }
        return readLogger;
    }

    public static Logger getWriterLogger() {
        if (writerLogger == null)
            try {
                writerLogger = new DefaultLogger(WRITERS_LOG_FILE, ENCODING);
            } catch (Exception e) {
                throw new IllegalStateException("Failed to initialize loggers", e);
            }

        return writerLogger;
    }

    private static class DefaultLogger implements Logger {
        private PrintWriter writerLogs;

        private DefaultLogger(String fileName, String encoding)
                throws IOException {
            this.writerLogs = new PrintWriter(new FileWriter(fileName));
        }

        @Override
        public void put(String logStatement) {
            try {
                writerLogs.println(logStatement);
                writerLogs.flush();
            } catch (Exception e) {
                throw new IllegalStateException("Failed to initialize loggers", e);
            }
        }
    }
}
