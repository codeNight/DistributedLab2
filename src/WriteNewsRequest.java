import java.io.Serializable;

public class WriteNewsRequest implements Serializable {
    private static final long serialVersionUID = 127L;


    private int clientId;
    private int news;

    public WriteNewsRequest(int clientId, int news) {
        this.news = news;
        this.clientId = clientId;
    }

    public int getNews() {
        return news;
    }

    public void setNews(int news) {
        this.news = news;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }
}
