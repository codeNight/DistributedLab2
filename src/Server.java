import java.rmi.AlreadyBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class to start the server and bind all the remote skeletons to their namespaces.
 *
 * args format : <ipAddress> <portNumber>
 */
public class Server {
    private static final int IP_ADDRESS = 0;
    private static final int PORT_NUMBER = 1;

    private static final String HOSTNAME_SYSTEM_PROPERTY = "java.rmi.server.hostname";

    public static void main(String[] args) {
        System.out.println("Running server");
        if(args.length != 2) {
            throw new IllegalArgumentException("You should provide 2 args: <host name> <port number>");
        }

        String ipAddress = args[IP_ADDRESS];
        int portNumber = Integer.parseInt(args[PORT_NUMBER]);

        System.setProperty(HOSTNAME_SYSTEM_PROPERTY, ipAddress);


        if (System.getSecurityManager() == null)
            System.setSecurityManager(new SecurityManager());

        try {
            AtomicInteger serSeqNum = new AtomicInteger(0);
            AtomicInteger reqSeqNum = new AtomicInteger(0);
            AtomicInteger news = new AtomicInteger(-1);

            // Exporting remote objects
            ReadNews readApi = (ReadNews) UnicastRemoteObject
                    .exportObject(new ReadNewsImpl(news, reqSeqNum, serSeqNum), 0);

            WriteNews writeAPI = (WriteNews) UnicastRemoteObject
                    .exportObject(new WriteNewsImpl(news, reqSeqNum, serSeqNum), 0);

            Registry registry = LocateRegistry.createRegistry(portNumber);

            // Binding objects to namespace
            registry.bind(ReadNews.READ_NEWS_API_REGISTRY_NAME, readApi);
            registry.bind(WriteNews.WRITE_NEWS_API_REGISTRY_NAME, writeAPI);

            System.out.println("Server ready");

        } catch (RemoteException e) {
            System.err.println("Failed to export remote object(s) for public server. Abort." + e.getMessage());
            e.printStackTrace();
        } catch (AlreadyBoundException e) {
            System.err.println("Failed to bind object(s) to given string." + e.getMessage());
            e.printStackTrace();
        }
    }
}
