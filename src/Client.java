import java.io.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Random;

/**
 * Class used to start a client instance on a machine.
 */
public class Client {
    public static final int MAX_MS = 1000;

    private static final String WRITER = "Writer";
    private static final String READER = "Reader";

    private static final int HOST_NAME = 0;
    private static final int PORT_NUMBER = 1;
    private static final int NUM_OF_ACCESSES = 2;
    private static final int ID = 3;
    private static final int CLIENT_TYPE = 4;

    private static final int READER_CLIENT = 1;

    private static final Random randomGenerator = new Random();

    public static void main(String[] args) throws IOException {
        System.out.println("Running client");
        if (args.length != 5) {
            throw new IllegalArgumentException("You should provide 5 args: <host name> "
                    + "<port number> <num of accesses> <ID> <is reader (1/0)>");
        }

        String hostName = args[HOST_NAME];
        int portNumber = Integer.parseInt(args[PORT_NUMBER]);
        int numOfAccesses = Integer.parseInt(args[NUM_OF_ACCESSES]);
        int id = Integer.parseInt(args[ID]);
        int clientType = Integer.parseInt(args[CLIENT_TYPE]);

        PrintWriter fileWriter = initiateClientLogFile(id, clientType);

        // Getting the registry
        Registry registry = LocateRegistry.getRegistry(hostName, portNumber);

        try {
            // Getting remote objects
            WriteNews writeNews = (WriteNews) registry.lookup(WriteNews.WRITE_NEWS_API_REGISTRY_NAME);
            ReadNews readNews = (ReadNews) registry.lookup(ReadNews.READ_NEWS_API_REGISTRY_NAME);

            for (int i = 0; i < numOfAccesses; i++) {
                if (clientType == READER_CLIENT) {
                    ReadNewsResponse response = readNews.getNews(new ReadNewsRequest(id));

                    printReaderClientLog(
                            response.getReqSeqNum(), response.getSerSeqNum(), response.getNews(), fileWriter);
                } else {
                    WriteNewsResponse response = writeNews.writeNews(new WriteNewsRequest(id, id));

                    printWriterClientLog(response.getReqSeqNum(), response.getSerSeqNum(), fileWriter);
                }

                if (i != numOfAccesses - 1) {    // not the last access
                    Thread.sleep(randomGenerator.nextInt(MAX_MS));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static PrintWriter initiateClientLogFile(int id, int clientType)
            throws FileNotFoundException, UnsupportedEncodingException {

        PrintWriter fileWriter = new PrintWriter(String.format("log_%d_%d.txt",id, clientType), "UTF-8");

        if(clientType == READER_CLIENT) {
            fileWriter.println("Client type: " + READER);
            fileWriter.println("Client Name: " + id);
            fileWriter.println("rSeq\tsSeq\toVal");
        } else {
            fileWriter.println("Client type: " + WRITER);
            fileWriter.println("Client Name: " + id);
            fileWriter.println("rSeq\tsSeq");
        }

        return fileWriter;
    }

    private static void printReaderClientLog(int rSeq, int sSeq, int oVal, PrintWriter fileWriter) {
        System.out.println(String.format("%d\t\t%d\t\t%d", rSeq, sSeq, oVal));
        printToFile(fileWriter, String.format("%d\t\t%d\t\t%d", rSeq, sSeq, oVal));
    }

    private static void printWriterClientLog(int rSeq, int sSeq, PrintWriter fileWriter) {
        System.out.println(String.format("%d\t\t%d", rSeq, rSeq));
        printToFile(fileWriter, String.format("%d\t\t%d", rSeq, rSeq));
    }

    private static void printToFile(PrintWriter fileWriter, String text) {
        fileWriter.println(text);
        fileWriter.flush();
    }
}
